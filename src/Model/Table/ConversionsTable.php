<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Conversions Model
 *
 * @method \App\Model\Entity\Conversion get($primaryKey, $options = [])
 * @method \App\Model\Entity\Conversion newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Conversion[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Conversion|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Conversion saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Conversion patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Conversion[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Conversion findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ConversionsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('conversions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->date('startdate')
            ->requirePresence('startdate', 'create')
            ->allowEmptyDate('startdate', false);

        $validator
            ->date('enddate')
            ->requirePresence('enddate', 'create')
            ->allowEmptyDate('enddate', false);

        $validator
            ->integer('daysbetween')
            ->allowEmptyString('daysbetween');

        return $validator;
    }
}
