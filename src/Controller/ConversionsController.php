<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Conversions Controller
 *
 * @property \App\Model\Table\ConversionsTable $Conversions
 *
 * @method \App\Model\Entity\Conversion[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ConversionsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $conversions = $this->paginate($this->Conversions);

        $this->set(compact('conversions'));
    }

    /**
     * View method
     *
     * @param string|null $id Conversion id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $conversion = $this->Conversions->get($id, [
            'contain' => []
        ]);

        $this->set('conversion', $conversion);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $conversion = $this->Conversions->newEntity();
       
        if ($this->request->is('post')) {

            $conversion = $this->Conversions->patchEntity($conversion, $this->request->getData());
         
            $daysbetween = $this->_calc($conversion);
            $conversion->set('daysbetween',$daysbetween);
           
            if ($this->Conversions->save($conversion)) {
                $this->Flash->success(__('The conversion has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The conversion could not be saved. Please, try again.'));
        }
        $this->set(compact('conversion'));
    }
    public function _calc($conversion){
        $first = explode("/",$conversion->startdate);
        $second = explode("/",$conversion->enddate);
        
        
        $m1 = $first[0];
        $d1 = $first[1];
        $y1 = $first[2];
        
        $m2 = $second[0];
        $d2 = $second[1];
        $y2 = $second[2];
       
        
        return $this->_getDifference($m1, $d1, $y1, $m2, $d2, $y2) ;  
}
        public function _isLeapYear($year) {
            return ($year % 4 == 0) && ( ($year % 100 != 0) || ($year % 400 == 0) );
            }
    
            public function _isDate1BeforeThanDate2($m1, $d1, $y1, $m2, $d2, $y2){               
                if($y1 < $y2) return true;
                if($y1 == $y2) return ($m1 < $m2) || (($m1 == $m2) && ($d1 < $d2));
                return false;
            }
            public function  _isLastMonthInYear($month)
            {
                return ($month == 12);
            }
            public function _increaseDateByOneDay(&$year, &$month, &$day){
                if ($this->_isonLastDayInMonth($year, $month, $day))
                {
                  if ($this->_isLastMonthInYear($month))
                  {
                    $month = 1;
                    $day = 1;
                    $year++;
                  }
                  else
                  {
                    $day = 1;
                    $month++;
                  }
                }
                else
                {
                  $day++;
                }
            } 
            public function _isonLastDayInMonth($year, $month, $day) { 
                 $daysPerMonth = array( 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 );
                 $daysPerMonthLeapYear = array( 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 );
                if ($this->_isLeapYear($year))
                {
                    return ($daysPerMonthLeapYear[$month-1] == $day);
                }
                else
                {
                    return ($daysPerMonth[$month-1] == $day);
                }
                return;
        }
    
            public function  _getDifference($m1, $d1, $y1, $m2, $d2, $y2){
            /* save preceding date to class variables which are modified by IncreaseDateByOneDay */
            $year = $y1;
            $month = $m1;
            $day = $d1;
            $days_count = 0;
            
            while ($this->_isDate1BeforeThanDate2( $month,$day,$year,  $m2, $d2, $y2))
            {
                $days_count++;
                $this->_increaseDateByOneDay($year, $month, $day); 
            }
            
            return $days_count;
            } 
              public function  _isLastDayInMonth($year, $month, $day){ /* check for month in valid range [1, 12] discarded here */
                return (($month == 1 && $day == 31) ||
                        ($this->_isLeapYear($year) ? ($month == 2 && $day == 29) : ($month == 2 && $day == 28)) ||
                        ($month == 3 && $day == 31) ||
                        ($month == 4 && $day == 30) ||
                        ($month == 5 && $day == 31) ||
                        ($month == 6 && $day == 30) ||
                        ($month == 7 && $day == 31) ||
                        ($month == 8 && $day == 31) ||
                        ($month == 9 && $day == 30) ||
                        ($month == 10 && $day == 31) ||
                        ($month == 11 && $day == 30) ||
                        ($month == 12 && $$month == 31)
               );
              } 
              
    /**
     * Edit method
     *
     * @param string|null $id Conversion id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $conversion = $this->Conversions->get($id, [
            'contain' => []
        ]);


        if ($this->request->is(['patch', 'post', 'put'])) {
            $conversion = $this->Conversions->patchEntity($conversion, $this->request->getData());
            $daysbetween = $this->_calc($conversion);
            $conversion->set('daysbetween',$daysbetween);
            if ($this->Conversions->save($conversion)) {
                $this->Flash->success(__('The conversion has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The conversion could not be saved. Please, try again.'));
        }
        $this->set(compact('conversion'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Conversion id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $conversion = $this->Conversions->get($id);
        if ($this->Conversions->delete($conversion)) {
            $this->Flash->success(__('The conversion has been deleted.'));
        } else {
            $this->Flash->error(__('The conversion could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
