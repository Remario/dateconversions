<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Conversion $conversion
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $conversion->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $conversion->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Conversions'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="conversions form large-9 medium-8 columns content">
    <?= $this->Form->create($conversion) ?>
    <fieldset>
        <legend><?= __('Edit Conversion') ?></legend>
        <?php
            echo $this->Form->control('startdate');
            echo $this->Form->control('enddate');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
