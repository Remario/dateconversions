<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Conversion $conversion
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Conversion'), ['action' => 'edit', $conversion->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Conversion'), ['action' => 'delete', $conversion->id], ['confirm' => __('Are you sure you want to delete # {0}?', $conversion->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Conversions'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Conversion'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="conversions view large-9 medium-8 columns content">
    <h3><?= h($conversion->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($conversion->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Daysbetween') ?></th>
            <td><?= $this->Number->format($conversion->daysbetween) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($conversion->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($conversion->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Startdate') ?></th>
            <td><?= h($conversion->startdate) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Enddate') ?></th>
            <td><?= h($conversion->enddate) ?></td>
        </tr>
    </table>
</div>
