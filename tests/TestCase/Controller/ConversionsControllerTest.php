<?php
namespace App\Test\TestCase\Controller;

use App\Controller\ConversionsController;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;
use Cake\ORM\TableRegistry;
use App\Model\Table\ConversionsTable;
/**
 * App\Controller\ConversionsController Test Case
 */
class ConversionsControllerTest extends TestCase
{
    
    use IntegrationTestTrait;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Conversions'
    ];
    public function setUp()
    {
        parent::setUp();
        $this->Conversions = TableRegistry::getTableLocator()->get('Conversions');
    }

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
    $controller = new ConversionsController();
    $assertions = Array(23,30,36,768,4);
       $results =  $this ->Conversions->find('all');
        foreach ($results as $conversion) {
            $result = ($controller->_calc($conversion));
            $this->assertContains($result, $assertions);
        }        
    }


       
    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
