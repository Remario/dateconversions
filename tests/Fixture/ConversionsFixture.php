<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ConversionsFixture
 */
class ConversionsFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'startdate' => ['type' => 'date', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'enddate' => ['type' => 'date', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'daysbetween' => ['type' => 'integer', 'length' => 10, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8mb4_0900_ai_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
            'id' => 1,
            'created' => '2019-04-22 12:49:31',
            'modified' => '2019-12-20 12:49:31',
            'startdate' => '2019/05/7',
            'enddate' => '2019/05/30',
            ],
            [ 
            'id' => 2,
            'created' => '2019-04-22 12:49:31',
            'modified' => '2019-12-20 12:49:31',
            'startdate' => '2000/02/29',
            'enddate' => '2000/03/30'],
            [               
            'id' => 3,
            'created' => '2019-04-22 12:49:31',
            'modified' => '2019-12-20 12:49:31',
            'startdate' => '2019/07/7',
            'enddate' => '2019/08/12',],
            [               
            'id' => 4,
            'created' => '2019-04-22 12:49:31',
            'modified' => '2019-12-20 12:49:31',
            'startdate' => '2015/09/8',
            'enddate' => '2017/10/15',],
            [               
            'id' => 5,
            'created' => '2019-04-22 12:49:31',
            'modified' => '2019-12-20 12:49:31',
            'startdate' => '2010/12/30',
            'enddate' => '2012/03/13',]
            
        ];
        parent::init();
    }
}
